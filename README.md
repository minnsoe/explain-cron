# explain-cron

`explain-cron` is a single file Python CLI programme used to break down the trigger points of a cron expression. It is used to output the individual time points when the command expected to run, broken down by the unit of time. The programme currently only supports the following time units:

* minute
* hour
* day of month
* month
* day of week

**Example output:**

```
>> python3 explain_cron.py "15 15-20 */5 * * /bin/test"

minute          15            
hour            15 16 17 18 19 20
day of month    1 6 11 16 21 26 31
month           1 2 3 4 5 6 7 8 9 10 11 12
day of week     1 2 3 4 5 6 7 
command         /bin/test     
```

## Installing

To run this application, you must have `python3` installed. Currently, this application is only supported on Linux based operating systems.

### Using as Python script

If you want to use this as a Python script without modifying your Linux environment, you can clone this repo and use the script directly:

```sh
>> python3 explain_cron.py "15 15-20 */5 * * /bin/test"
```

### Using as shell script

**If this repo is public**

If this repo is public and you can access it from the target Linux machine, you can install the latest version of this programme in your user directory by running the following in your CLI (**warning**: make sure you trust the source of this message before running this command!):

```sh
wget -O - https://raw.githubusercontent.com/sansaid/explain-cron/v0.1.0/install.sh | bash
```

**If this repo is private**

If this repo is private and you are added as a contributor to this repo, you need to clone this repo then run `./install.sh local` in the root of the repo.

You must add `~/.explain-cron/bin` to your `PATH` variable manually (this is left up to you to do, in case you would like to only temporarily change the path for testing).

To test that this has worked successfully, run:

```sh
>> explain-cron -h

usage: explain-cron [-h] expression

Human friendly cron expression explainer

positional arguments:
  expression  The cron expression you want to explain to the human

optional arguments:
  -h, --help  show this help message and exit
```

## Contributing

See [the contributing docs](./CONTRIBUTING.md).